<?php

use Illuminate\Database\Seeder;

use App\Models\User;
use App\Models\UsersData;

class TestUsersAndUniversitiesSeeder extends Seeder
{
    const COUNT_UNIVERSITIES = 100; //с локациями
    const COUNT_EMPTY_UNIVERSITIES = 5; // без локации
    const COUNT_STUDENTS = 100; //студенты с анкетами
    const COUNT_EMPTY_STUDENTS = 5; // студенты без анкет

    protected $password;
    protected $faker;
    protected $table_values;
    protected $rus_lang_id;
    protected $russia_id;
    protected $count_rating;

    public function __construct()
    {
        $this->password = bcrypt('111111');
        $this->faker = Faker\Factory::create('ru_RU');
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->addMainUsers();
        $this->addStudents();
        $this->addUniversities();
        $this->addUniversityUsers();
    }

    public function addUniversityUsers()
    {
        $universities = \App\Models\University::select('id')->get();

        for($i=0; $i<count($universities); $i++) {

            $u = $universities[$i];

            $count = rand(1, 5);

            for($j=0; $j<$count; $j++) {
                $user = new User();

                $user->email = '-';
                $user->name = '-';
                $user->password = $this->password;
                $user->active = 1;
                $user->is_university = 1;
                $user->university_id = $u->id;

                $user->first_name = $this->faker->firstName;
                $user->middle_name = 'Иванович';
                $user->last_name = $this->faker->lastName;
                $user->phone1 = $this->faker->phoneNumber;
                $user->phone2 = $this->faker->phoneNumber;
                $user->university_contact = $j==0 && $i<count($universities)-1 ? 1 : 0;

                $user->save();

                $user->name = 'manager'.($user->id);
                $user->email = 'manager'.($user->id).'@manager.com';

                $user->save();
            }
        }
    }
}
