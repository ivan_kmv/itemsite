<?php
namespace App\Services;

use App\Models\EmailTemplate;
use App\Traits\EmailReplaceParams;
use Mail;

class EmailService
{
    use EmailReplaceParams;

    const LEFT_SYMBOLS = "[";
    const RIGHT_SYMBOLS = "]";

    public static function send(string $alias, $to, $params = [], $view_name = 'emails.base', $mail_class_name = 'BaseMail')
    {
        if ($et = EmailTemplate::where('alias', $alias)->first()) {

            $data = self::replace($params, $et, ['body', 'subject']);

            $data['from'] = $et->from ? $et->from : config('mail.from.address');
            $data['name_from'] = $et->name_from ? $et->name_from : config('mail.from.name');
            $data['to'] = $to;
            $data['bcc'] = $et->bcc;

            $mail_class_name = 'App\Mail\\'.$mail_class_name;

            try {

                Mail::send(new $mail_class_name($data, $view_name));

                return Mail::failures();

            } catch (\Exception $e) {

                \Log::alert('Ошибка при отправке письма ', compact('alias', 'to', 'params'));

                report($e);

                return $e->getCode();
            }
        }

        \Log::alert('Ошибка при отправке письма: не найден шаблон в базе', compact('alias'));

        return false;
    }
}