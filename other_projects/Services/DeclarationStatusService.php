<?php
namespace App\Services;

use App\Models\DeclarationDocType;
use App\Models\DeclarationFile;
use App\Models\DeclarationSendException;
use App\Models\DS;
use App\Models\QS;
use App\Models\RefDeclarationUniversity;
use App\Models\UsersData;
use App\Repositories\DeclarationRepository;
use App\Repositories\UniversityRepository;
use Carbon\Carbon;
use DB;
use Auth;

class DeclarationStatusService
{
    protected $university_rep;
    protected $dec_rep;
    protected $redirect_service;

    public function __construct(UniversityRepository $universityRepository,
                                DeclarationRepository $declarationRepository,
                                RedirectOrRejectDeclaration $redirectOrRejectDeclaration)
    {
        $this->university_rep = $universityRepository;
        $this->dec_rep = $declarationRepository;

        $this->redirect_service = $redirectOrRejectDeclaration;
    }

    public function returnDeclarationByCandidate($dec, $comments)
    {

        DB::beginTransaction();

        try {

            $dec_next = null;

            if ($dec->is_preparatory_faculty) {
                $dec_next = $this->dec_rep->getCurDeclarationNext($dec->user_id);
            }

            $dec->status_id = DS::getInitCommonCandidateStatus();
            $dec->returned_comment = $comments;
            $dec->is_returned = 1; 
            $dec->save();

            if ($dec->is_preparatory_faculty) {

                $dec_next->status_id = DS::getInitCommonCandidateStatus();
                $dec_next->returned_comment = $comments; 
                $dec_next->is_returned = 1;
                $dec_next->save();
            }

            DB::commit();

        } catch(\Exception $e) {

            DB::rollback();

            \Log::alert('Ошибка при восстановлении заявки кандидатом '.$e->getMessage(),
                ['user_id' => $dec->user_id, 'declaration_id' => $dec->id]);

            return false;
        }

        return true;
    }
}