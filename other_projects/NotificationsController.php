<?php

namespace App\Http\Controllers;

use App\Events\ReadNotificationEvent;
use App\Models\Notification;
use App\Repositories\Notification\NotificationRepositoryInterface;

use Auth;
use Config;

class NotificationsController extends Controller {
    /**
     * @var NotificationRepositoryInterface
     */
    protected $notificationRepository;

    /**
     * NotificationsController constructor.
     * @param NotificationRepositoryInterface $notificationRepository
     */
    public function __construct(NotificationRepositoryInterface $notificationRepository) {
        $this->middleware('auth');
        $this->notificationRepository = $notificationRepository;

        parent::__construct();
    }

    public function all($page = 1) {
        return view('notifications', [
            'notifications' => $this->notificationRepository->getUserNotificationsBuilder(Auth::user())->paginate(Config::get('notifications.items_per_page'), ['*'], 'page', $page)
        ]);
    }

    /**
     * @param Notification $notification
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function view(Notification $notification) {
        event(new ReadNotificationEvent(Auth::user(), $notification->id));

        if (is_null($notification->href)) {
            return redirect()->back();
		}

        return redirect($notification->href);
    }
}