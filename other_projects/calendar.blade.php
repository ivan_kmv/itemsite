@extends('layouts.main')

@section('meta')
  <meta name="description" content="Стоимость аренды квартиры в Сочи, Адлере, Кудепсте">
  <meta name="keywords" content="квартира в аренду, недорогое жильё на море, снять квартиру в Адлере, аренда квартиры в Кудепсте">
@endsection

 @section('css')
     <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
     <link rel="stylesheet" href="{{ asset('admin/libs/plugins/fullcalendar/fullcalendar.min.css') }}">
     <link rel="stylesheet" href="{{ asset('admin/libs/plugins/fullcalendar/fullcalendar.print.min.css') }}" media="print">
     <style>
         #calendar .fc-today {
             background: none;
         }
         #calendar .fc-title {
             margin: 50% auto;
             color: gray;
         }
         #calendar .fc-time {
             display:none;
         }
         #calendar a.fc-day-grid-event {
             background-color: transparent;
             border: none;
             text-align: center;
             padding-top: 14px;
         }
         #calendar .fc-scroller {
             overflow-y: hidden !important;
         }

         #calendar td {
             cursor: pointer;
         }

         #calendar .fc-content-skeleton {
             z-index: 1;
         }

         #calendar .fc-bg {
             z-index: 999;
         }
     </style>
 @endsection

 @section('content')
 <main class="page-content">
     <h2 class="text-center">Календарь</h2>
    <div class="row">
      <section class="col-lg-6 col-md-6 ">
        <div class="container">
            <div id='calendar'></div>
        </div>
      </section>
     <section class="col-lg-6 col-md-6">
        <div class="container">
            <div class="row square">
                 <div class="col-lg-12" style="margin: 20px 0 0 15px;">
                  <p>При клике на дату в календаре выбирается дата приезда, при следующем клике - дата выезда.</p>
                </div>
                 <div class="col-lg-4 col-md-6 col-xs-6" >
                    <div class="square-1"></div> 
                    <span class="square-text-1">свободно</span> 
                </div>
                <div class="col-lg-8 col-md-6 col-xs-6">
                    <div class="square-2" style="background-color: #ffb6ad; border: 1px solid black; margin-left: 15px;"></div> 
                    <span>занято</span>    
                </div>
            </div>
            <div class="col-lg-12" style="margin-bottom: -30px;">
                <p>Расчётное время приезда 14-00, выезда 12-00</p>
            </div>
            <div class="col-lg-12">
                <p id="error_message"></p>
                <form method="post" id="req_form" action="{{route('save_order_calendar')}}">
                    {{ csrf_field() }}
                      <div class="row">
                        <div class="form-group col-lg-4">
                          <label for="rd-mailform-time" id="rd-mailform-time-label" class="form-label">Дата заезда:</label>
                           <input id="rd-mailform-time" type="text" name="check-in" value="{{old('check-in')}}" data-time-picker="date" data-constraints="@Required" class="form-control">
                        </div>
                        <div class="form-group col-lg-4">
                          <label for="rd-mailform-time-1" id="rd-mailform-time-1-label" class="form-label">Дата выезда:</label>
                          <input id="rd-mailform-time-1" type="text" name="check-out" value="{{old('check-out')}}" data-time-picker="date" data-constraints="@Required" class="form-control">
                        </div>
                        <div class="form-group col-lg-4" style="margin-top: -5px;">
                          <select id="count" name="count" data-placeholder="1 Гость" data-minimum-results-for-search="Infinity" data-constraints="" class="form-control select-filter col-lg-4">
                            <option value="1" @if(old('count') == 1) selected @endif>1 Гость</option>
                            <option value="2" @if(old('count') == 2) selected @endif>2 Гостя</option>
                            <option value="3" @if(old('count') == 3) selected @endif>3 Гостя</option>
                            <option value="4" @if(old('count') == 4) selected @endif>4 Гостя</option>
                            <option value="5" @if(old('count') == 5) selected @endif>5 Гостей</option>
                          </select>
                        </div>
                     </div>
                     <div class="row" style="margin-top: -20px; margin-bottom: -40px; display: none;" id="div_price">
                        <div class="col-lg-6">
                            <p>Количество дней пребывания: <span id="count_days">-</span></p>
                        </div>
                        <div class="col-lg-6">
                            <p>Сумма вашего бронирования: <span id="price">-</span> руб</p>
                        </div>
                     </div>
                     <div class="row" style="margin-top: 30px;">
                     <div class="col-md-12" id="div_error_form">
                         @if(count($errors))
                             <p style="color: #ed2d2d;">{{$errors->first()}}</p>
                         @endif
                     </div>
                     <div class="col-md-4">
                          <div class="form-group">
                            <label for="contact-name-1" class="form-label">Имя</label>
                            <input id="contact-name-1" type="text" name="name" data-constraints="@Required" value="{{old('name')}}" class="form-control">
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="contact-phone-2" class="form-label">Телефон</label>
                            <input id="contact-phone-2" type="text" name="phone" data-constraints="@Numeric" value="{{old('phone')}}" class="form-control">
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="contact-email-1" class="form-label">E-Mail</label>
                            <input id="contact-email-1" type="email" name="email" data-constraints="@Email" value="{{old('email')}}" class="form-control">
                          </div>
                        </div>
                        <div class="col-md-12 offset-top-0">
                          <div class="form-group">
                            <label for="contact-message-3" class="form-label">Сообщение</label>
                            <textarea id="contact-message-3" name="message"  class="form-control">{{old('message')}}</textarea>
                          </div>
                        </div>

                         <div class="col-md-8">
                             <div class="form-group">
                                 <input type="text" name="captcha" class="form-control" placeholder="Числа с картинки" required value="">
                             </div>
                         </div>
                         <div class="col-md-4">
                         <div class="form-group">
                             {!! captcha_img('flat') !!}
                             </div>
                         </div>
                      <div class="col-md-12" >
                        <button type="submit" class="btn btn-md--inset-2 btn-secondary btn-kamil-secondary offset-top-22" style="margin-top: -10px;"><span>Отправить</span></button>
                      </div>
                      <p>Нажимая на кнопку, Вы даете <a href="{{route('politik')}}">согласие на обработку своих персональных данных</a></p>
                      </div>
                </form>
            </div>    
        </div>
     </section>
    </div>  
</main>

 <div id="modal_q" class="modal fade" style="top: 150px;">
     <div class="modal-dialog modal-md">
         <div class="modal-content">
             <div class="modal-header">
                 <span class="modal-title">Сообщение</span>
             </div>
             <div class="modal-body" style="padding-left: 40px; padding-right: 40px;">
                 <p id="modal_text">

                 </p>
             </div>
             <div class="modal-footer">
                 <button type="button" class="btn" data-dismiss="modal">Закрыть</button>
             </div>
         </div>
     </div>
 </div>
@endsection

@section('js')
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('admin/libs/plugins/moment/moment.js') }}"></script>
    <script src="{{ asset('admin/libs/plugins/fullcalendar/fullcalendar.min.js') }}"></script>
    <script src="{{ asset('admin/libs/plugins/fullcalendar/locale/ru.js') }}"></script>
    <script>
        $(document).ready(function(){

            var div_price = $('#div_price');
            var div_error_form = $('#div_error_form');

            $('#req_form').submit(function(){

                if($('#contact-email-1').val().trim() == '' && $('#contact-phone-2').val().trim() == '') {
                    div_error_form.html('<p style="color: #ed2d2d;">Пожалуйста, укажите телефон или email</p>').show();
                    return false;
                }

                if($('#rd-mailform-time').val().trim() == '') {
                    div_error_form.html('<p style="color: #ed2d2d;">Пожалуйста, укажите дату заезда</p>').show();
                    return false;
                }

                if($('#rd-mailform-time-1').val().trim() == '') {
                    div_error_form.html('<p style="color: #ed2d2d;">Пожалуйста, укажите дату выезда</p>').show();
                    return false;
                }
            });

            var date_start = $('#rd-mailform-time').val();
            var date_end = $('#rd-mailform-time-1').val();

            if(date_start != '' && date_end != '') {
                getPrice();
            }
            else if (localStorage.getItem("checkin") !== null)
            {
               var checkin = localStorage.getItem('checkin');
               var checkout = localStorage.getItem('checkout');
               var count = localStorage.getItem('count');

               $('#rd-mailform-time-label').text('');
               $('#rd-mailform-time-1-label').text('');

               $('#rd-mailform-time').val(checkin);
               $('#rd-mailform-time-1').val(checkout);

               $('#count').val(count).trigger('change');

               getPrice();

               localStorage.removeItem('checkin');
               localStorage.removeItem('checkout');
               localStorage.removeItem('count');
            }

            function cleanFormErrors()
            {
                div_error_form.html('');
            }

            var is_message = false;

            @if(session()->has('message'))
                is_message = true;
                $('#modal_text').css('color','#000').text('{{session('message')}}');
            @endif

            @if(session()->has('error'))
                is_message = true;
                $('#modal_text').css('color','#ed2d2d').text('{{session('error')}}');
            @endif

            if(is_message) {
                $('#modal_q').modal();
            }

            var reqs = {!! $reqs !!};

            var count_click = 0;

            $('#calendar').fullCalendar({
                header    : {
                    left  : 'prev,next today',
                    center: 'title',
                    right : ''
                },

                events: reqs,

                borderColor: '#ed2d2d',

                dayClick: function(date) {
                    cleanFormErrors();

                    var day = date.format('DD.MM.Y');

                    var date_start = $('#rd-mailform-time').val();
                    var date_end = $('#rd-mailform-time-1').val();

                    if(date_start == '') {
                        $('#rd-mailform-time-label').text('');
                        $('#rd-mailform-time').val(day);
                        count_click = 0;
                    } else if (date_end == '') {
                        $('#rd-mailform-time-1-label').text('');
                        $('#rd-mailform-time-1').val(day);
                        count_click = 0;
                    } else {
                        count_click++;
                        if(count_click == 3) {
                            count_click = 1;
                        }

                        if(count_click == 1) {
                            $('#rd-mailform-time-label').text('');
                            $('#rd-mailform-time').val(day);
                        } else {
                            $('#rd-mailform-time-1-label').text('');
                            $('#rd-mailform-time-1').val(day);
                        }
                    }

                    getPrice();
                }

            });

            // algo

            $('#rd-mailform-time').change(getPrice);
            $('#rd-mailform-time-1').change(getPrice);
            $('#count').change(getPrice);

            function getPrice()
            {
                div_price.html('').hide();

                var date_start = $('#rd-mailform-time').val();
                var date_end = $('#rd-mailform-time-1').val();
                var count = $('#count').val();

                if(date_end == '') {
                    div_price.html('<div class="col-lg-12"><p>Пожалуйста, укажите дату выезда</p></div>').show();
                    return false;
                }

                if(date_start == ''){
                    div_price.html('<div class="col-lg-12"><p>Пожалуйста, укажите дату заезда</p></div>').show();
                    return false;
                }

                var m_start = moment(date_start, "DD.MM.YYYY");
                var m_end = moment(date_end, "DD.MM.YYYY");

                if(!m_start.isBefore(m_end)) {
                    div_price.html('<div class="col-lg-12"><p style="color: #ed2d2d;">Дата выезда должна быть позже даты заезда.</p></div>').show();
                    return false;
                }

                var _token = $('input[name=_token]').val();

                $.ajax({
                    url: '{{ route('ajax-calendar-price') }}',
                    headers: {'X-CSRF-TOKEN': _token},
                    data: {
                        '_token': _token,
                        'date_start': date_start,
                        'date_end': date_end,
                        'count': count
                    },
                    type: 'POST',
                    datatype: 'JSON',
                    success: function (resp) {
                        if(resp.result) {
                            div_price.html('<div class="col-lg-6"><p>Количество дней пребывания: ' + resp.days + '</p></div><div class="col-lg-6"><p>Сумма вашего бронирования: ' + resp.price + ' руб</p>').show();
                        } else {
                            div_price.html('<div class="col-lg-12"><p style="color: #ed2d2d;">'+resp.message+'</p></div>').show();
                        }
                        cleanFormErrors();
                    },
                    error: function(){
                        alert('Сервер временно недоступен');
                    }
                });

            }
        });
    </script>
@endsection