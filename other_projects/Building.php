<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Building extends Model
{
    protected $table = 'buildings';
    protected $guarded = ['id'];

    public function rooms()
    {
        return $this->hasMany(Room::class)->orderBy('name');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
