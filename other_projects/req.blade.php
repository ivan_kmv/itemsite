@extends('manager._template')

@section('title','Заявка')

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/libs/plugins/daterangepicker/daterangepicker.css') }}">
@endsection

@section('content_header')
    <section class="content-header">
        <h1>
            Заявка № {{ $req->req_id }}
        </h1>
    </section>
@endsection

@section('content_body')
    <section class="content">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-10">
                <div class="box">
                    <form role="form" id="form" action="" method="post" enctype="multipart/form-data">
                        @if (count($errors)>0)
                            <div id="error_profile" class="alert alert-danger alert-dismissible" style="border-radius: 0;">
                                {{ $errors->first() }}
                            </div>
                        @endif
                        {{ csrf_field() }}
                        <div class="box-body">
                            <div class="form-group">
                                <label>ФИО клиента</label><br>
                                <a href="{{url('manager/client',[$req->user->user_id])}}">{{ $req->user->name }}</a>
                            </div>
                            <div class="form-group">
                                <label>Телефон клиента</label>
                                <input type="text" disabled class="form-control" placeholder="" value="{{ $req->user->phone }}">
                            </div>
                            <div class="form-group">
                                <label for="dates">Период проживания</label>
                                <div class="input-group col-md-8 col-lg-6">
                                    <input  type="text"
                                            class="form-control"
                                            name="dates"
                                            id="dates" placeholder="">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Номер</label>
                                <select name="room" class="form-control">
                                    @foreach($rooms as $r)
                                        <option value="{{$r->room_id}}" @if((old('room') && old('room') == $r->room_id) || (!old('room') && $req->room_id==$r->room_id))  {{ 'selected' }}@endif>{{ $r->number }} ({{$r->rus_type}})</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Количество человек</label>
                                <input type="number" min="1" max="5" required class="form-control" name="count" placeholder="" value="@if(old('count')!==null){{ old('count') }}@else{{ $req->count }}@endif">
                            </div>
                            <div class="form-group">
                                <label>Стоимость</label>
                                <p>{{ $req->total_price }} руб</p>
                            </div>
                            <div class="form-group">
                                <label>Подробности от клиента</label>
                                <textarea id="desc" name="description" class="form-control" rows="12">{{ $req->description }}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Заметки менеджера</label>
                                <textarea id="comments" name="comments" class="form-control" rows="12">{{ $req->comments }}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Статус заявки</label>
                                <select name="status" class="form-control">
                                    <option value="1" @if((old('status') && old('status') == 1) || (!old('status') && $req->status==1))  {{ 'selected' }}@endif>новая</option>
                                    <option value="2" @if((old('status') && old('status') == 2) || (!old('status') && $req->status==2))  {{ 'selected' }}@endif>в процессе</option>
                                    <option value="3" @if((old('status') && old('status') == 3) || (!old('status') && $req->status==3))  {{ 'selected' }}@endif>закрыта</option>
                                    <option value="4" @if((old('status') && old('status') == 3) || (!old('status') && $req->status==4))  {{ 'selected' }}@endif>отменена</option>
                                </select>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary" style="width: 220px;">Сохранить изменения</button>&nbsp;&nbsp;&nbsp;
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('js')
    <script src="{{ asset('assets/libs/plugins/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('assets/js/options.js') }}"></script>

    <script src="{{asset('assets/libs/plugins/daterangepicker/moment.min.js')}}"></script>
    <script src="{{asset('assets/libs/plugins/daterangepicker/daterangepicker.js')}}"></script>
    <script>
        var date_opt1 = {
            "timePicker": true,
            "locale": {
                "format": "DD.MM.YYYY HH:mm",
                "separator": " - ",
                "applyLabel": "Сохранить",
                "cancelLabel": "Отмена",
                "fromLabel": "От",
                "toLabel": "До",
                "customRangeLabel": "Custom",
                "weekLabel": "W",
                "daysOfWeek": [
                    "Вс","Пн","Вт","Ср","Чт","Пт","Сб"
                ],
                "monthNames": [
                    "Январь","Февраль","Март","Апрель","Май","Июнь","Июль","Август","Сентябрь","Октябрь",
                    "Ноябрь","Декабрь"
                ],
                "firstDay": 1
            },
            "linkedCalendars": false,
            "timePicker24Hour": true,
            "opens": "center"
        };

        date_opt1['startDate'] = "@if(old('count')!==null){{ trim(explode("-",old('dates'))[0]) }}@else{{ $date[0] }}@endif";
        date_opt1['endDate'] = "@if(old('count')!==null){{ trim(explode("-",old('dates'))[1]) }}@else{{ $date[1] }}@endif";


        $('#dates').daterangepicker(date_opt1);
    </script>
@endsection