@extends('user._template')

@section('title','Объекты')

@section('content_header')
    <section class="content-header">
        <h1>Объекты</h1>
    </section>
@endsection

@section('content_body')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <button id="btn_add" class="btn btn-success btn-sm" role="button">Добавить объект</button>
                <div class="box-">
                    <div class="box-body" style="overflow-x: auto;">
                        {{ csrf_field() }}
                        @if (count($buildings)>0)
                        <table id="msg2" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th style="width: 30px;"></th>
                                    <th>Название</th>
                                    <th>Описание</th>
                                    <th>Помещения</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($buildings as $u)
                                    <tr>
                                        <td>{{ $u->id }}</td>
                                        <td>{{ $u->name }}</td>
                                        <td>{{ str_limit($u->description, 100) }}</td>
                                        <td>
                                            @foreach($u->rooms as $c)
                                                <a href="{{ route('user.room.edit',[$c->id]) }}" target="_blank">{{ $c->name }}</a>@if(!$loop->last), @endif
                                            @endforeach
                                        </td>
                                        <td>
                                            <button class="btn btn-success btn-xs add_btn" data-id="{{$u->id}}" role="button" data-toggle="tooltip" title="Добавить помещение"><i class="glyphicon glyphicon-plus"></i></button>
                                            <button class="btn btn-success btn-xs edit_btn" data-id="{{$u->id}}" data-name="{{ $u->name }}" data-description="{{ $u->description }}" role="button" data-toggle="tooltip" title="Изменить"><i class="glyphicon glyphicon-pencil"></i></button>
                                            <a class="btn btn-danger btn-xs" href="{{ route('user.building.delete', [$u->id]) }}" role="button" data-toggle="tooltip" title="Удалить"><i class="fa fa-close"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th style="width: 30px;"></th>
                                    <th>Название</th>
                                    <th>Описание</th>
                                    <th>Помещения</th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                        @else
                            <p>Объекты не найдены</p>
                        @endif
                    </div>
                </div>
                </div>
            </div>
    </section>

    <div id="modal_r" class="modal fade" style="top: 5px;">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <form class="form-horizontal"  method="post" action="{{route('user.room.add')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" id="r_id" name="building_id" value="">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">Новое помещение</h4>
                    </div>
                    <div class="modal-body" style="padding-left: 40px; padding-right: 40px;">
                        <div class="form-group">
                            <label>Название</label>
                            <input type="text" required name="name" class="form-control" placeholder="" value="">
                        </div>
                        <div class="form-group">
                            <label>Описание</label>
                            <input type="text"  name="description" class="form-control" placeholder="" value="">
                        </div>
                        <div class="form-group">
                            <label>Объем (м3)</label>
                            <input type="text" name="value" class="form-control" placeholder="" value="">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="modal_save_btn" class="btn btn-success">Сохранить</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="modal_q" class="modal fade" style="top: 5px;">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <form class="form-horizontal" id="modal_q_form" method="post" action="{{route('user.building.add')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">Новый объект</h4>
                    </div>
                    <div class="modal-body" style="padding-left: 40px; padding-right: 40px;">
                        <div class="form-group">
                            <label>Название</label>
                            <input type="text" required name="name" class="form-control" placeholder="" value="">
                        </div>
                        <div class="form-group">
                            <label>Описание</label>
                            <input type="text" name="description" class="form-control" placeholder="" value="">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" id="modal_save_btn" class="btn btn-success">Сохранить</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="modal_edit" class="modal fade" style="top: 5px;">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <form class="form-horizontal" id="modal_edit_form" method="post" action="{{route('user.building.edit')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">Изменение объекта</h4>
                    </div>
                    <input type="hidden" id="hidden_id" name="id" value="">
                    <div class="modal-body" style="padding-left: 40px; padding-right: 40px;">
                        <div class="form-group">
                            <label>Название</label>
                            <input type="text" id="m_edit_name" required name="name" class="form-control" placeholder="" value="">
                        </div>
                        <div class="form-group">
                            <label>Описание</label>
                            <input type="text" id="m_edit_description" name="description" class="form-control" placeholder="" value="">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Сохранить</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(function () {
            $("#msg2").DataTable({
                stateSave: true,
                "columns": [
                    null, null,
                    {"orderable": false },
                    {"orderable": false },
                    {"orderable": false },
                ]
            });

            $('.add_btn').click(function(){
                $('#r_id').val( $(this).attr('data-id') );
                $('#modal_r').modal();
            });

            $('#btn_add').click(function(){
                $('#modal_q').modal();
            });

            $('.edit_btn').click(function(){

                $('#hidden_id').val( $(this).attr('data-id') );
                $('#m_edit_name').val( $(this).attr('data-name') );
                $('#m_edit_description').val( $(this).attr('data-description') );

                $('#modal_edit').modal();
            });
        });
    </script>
@endsection