<?php

namespace App\Http\Middleware;

use App\Exceptions\UnconfirmedUserException;
use Closure;
use Auth;
use Exception;

class IsConfirmed
{
    /**
     * @param $request
     * @param Closure $next
     * @param null $guard
     * @return mixed
     * @throws Exception
     */
    public function handle($request, Closure $next, $guard = null) {
        if (Auth::check() && Auth::user()->is_confirmed == false) {
            throw new UnconfirmedUserException;
        }

        return $next($request);
    }
}
