<?php

namespace App\Repositories;

use App\Models\Citizenship;
use App\Models\City;
use App\Models\FederalDistrict;
use App\Models\Region;

class LocationRepository {

    public function isVisaCountry($country_id)
    {
        return Citizenship::where(['id' => $country_id, 'is_visa' => 1])->count() > 0;
    }

    public function getConsulateCitiesByCountry($country_id, $columns = ['id'], $order = 'id')
    {
        return City::where(['country_id' => $country_id, 'consulate' => 1])
            ->orderBy($order)->get($columns);
    }

    public function getCitiesByCountry($country_id, $columns = ['id'], $order = 'id')
    {
        return City::where('country_id', $country_id)
            ->orderBy($order)->get($columns);
    }

    public function getCity($city_id, $column = null)
    {
        if(is_null($column)) {
            return City::find($city_id);
        }

        return City::find($city_id)->$column;
    }

    public function getCityName($city_id)
    {
        return $this->getCity($city_id, 'name');
    }

    public function getCities($columns = ['*'], $order = null)
    {
        if (is_null($order)) {
            return City::get($columns);
        }

        return City::orderBy($order)->get($columns);
    }

    public function getCountries()
    {
        return Citizenship::orderBy('title')->get();
    }

    public function getFederalDistricts()
    {
        return  \Cache::rememberForever('federal_districts', function() {
            return FederalDistrict::orderBy('name')->get();
        });
    }

    public function getRegions()
    {
        return  \Cache::rememberForever('regions', function() {
            return Region::orderBy('name')->get();
        });
    }
}