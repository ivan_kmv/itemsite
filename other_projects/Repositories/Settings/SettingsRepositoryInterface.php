<?php

namespace App\Repositories\Settings;

interface SettingsRepositoryInterface {
    /**
     * @param $key
     * @param null $default
     * @return mixed
     */
    public function get($key, $default = null);

    /**
     * @param $key
     * @param $value
     * @return bool
     */
    public function set($key, $value);
}