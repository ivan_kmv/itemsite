<?php

namespace App\Repositories\Settings;

use App\Models\Setting;
use Cache;

class SettingsRepository implements SettingsRepositoryInterface {
    /**
     * @param $key
     * @param null $default
     * @return mixed
     */
    public function get($key, $default = null) {
        return Cache::remember("setting{key:$key}", 10, function() use($key, $default) {
            if ($setting = Setting::where('key', $key)->first())
                return $setting->value;

            return $default;
        });
    }

    /**
     * @param $key
     * @param $value
     * @return bool
     */
    public function set($key, $value) {
        Cache::forget("setting{key:$key}");

        if ($setting = Setting::where('key', $key)->first()) {
            return $setting->fill(['value' => $value])->save();
		}
		
        return Setting::create([
            'key' => $key,
            'value' => $value
        ])->save();
    }
}