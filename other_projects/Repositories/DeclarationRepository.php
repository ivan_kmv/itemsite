<?php

namespace App\Repositories;

use App\Models\Declaration;
use App\Models\DS;
use App\Models\QS;
use App\Models\RefDeclarationUniversity;
use App\Models\University;
use App\Models\User;

class DeclarationRepository
{
    public function getCurRefByDeclaration($declaration_id, $lock = 'no_lock')
    {
        $query = RefDeclarationUniversity::where('declaration_id', $declaration_id)
            ->where('queue', QS::CURRENT)
            ->where('archived', 0)
            ->orderBy('position');

        if ($lock === 'lock') {
            $query->lockForUpdate();
        }

        return $query->first();
    }

    public function getCurDeclarationNext($user_id, $lock = 'no_lock')
    {
        $query = Declaration::where(['user_id' => $user_id, 'year' => date('Y')+1]);

        if ($lock === 'lock') {
            $query->lockForUpdate();
        }

        return $query->first();
    }
	
    public function countUniversityDeclarationsFull($university_id, $year, $data)
    {
        $u_decs = University::where('id', $university_id)
            ->withCount(['ref_declarations as common_count' => function($q) use($data, $year) {
                        $q->whereHas('declaration', function($q2) use($data, $year) {
                            $q2->where($data)->where('year', $year);
                        })->whereIn('status', DS::getYearGroup());
                    }])
            ->withCount(['ref_declarations as distributed_count' => function($q) use($data, $year) {
                $q->whereHas('declaration', function($q2) use($data, $year) {
                    $q2->where($data)->where('year', $year);
                })->whereIn('status', DS::getDistributedGroup());
            }])
            ->first();

		return $u_decs;
    }
}