<?php

namespace Tests\Feature;

use App\Item;
use Tests\TestCase;

class ItemTest extends TestCase
{
    public function testsItemsAreCreatedCorrectly()
    {
        $data = [
            'name' => 'TestName',
            'key' => 'TestKey',
        ];

        $this->json('POST', '/api/items', $data)
            ->assertStatus(201)
            ->assertJson(['id' => 1, 'name' => 'TestName', 'key' => 'TestKey']);
    }

    public function testsItemsAreValidatedCorrectly()
    {
        $data = [
            'name' => 'TestName',
            'key' => 'TestKeyToooooooooooooooooooooLong',
        ];

        $this->json('POST', '/api/items', $data)
            ->assertStatus(422);
    }

    public function testsItemsAreUpdatedCorrectly()
    {
        $item = factory(Item::class)->create([
            'name' => 'TestName',
            'key' => 'TestKey',
        ]);

        $data = [
            'name' => 'TestName2',
            'key' => 'TestKey2',
        ];

        $this->json('PUT', '/api/items/' . $item->id, $data)
            ->assertStatus(200)
            ->assertJson([
                'id' => 1,
                'name' => 'TestName2',
                'key' => 'TestKey2'
            ]);
    }

    public function testsItemsAreDeletedCorrectly()
    {
        $item = factory(Item::class)->create([
            'name' => 'First Article',
            'key' => 'First Body',
        ]);

        $this->json('DELETE', '/api/items/' . $item->id, [])
            ->assertStatus(204);
    }

    public function testItemsAreListedCorrectly()
    {
        factory(Item::class)->create([
            'name' => 'First Article',
            'key' => 'First Body'
        ]);

        factory(Item::class)->create([
            'name' => 'Second Article',
            'key' => 'Second Body'
        ]);

        $this->json('GET', '/api/items', [])
            ->assertStatus(200)
            ->assertJson([
                [ 'name' => 'First Article', 'key' => 'First Body' ],
                [ 'name' => 'Second Article', 'key' => 'Second Body' ]
            ])
            ->assertJsonStructure([
                '*' => ['id', 'name', 'key', 'created_at', 'updated_at'],
            ]);
    }
}
