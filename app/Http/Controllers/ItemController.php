<?php

namespace App\Http\Controllers;

use App\Http\Requests\ItemRequest;
use App\Item;

class ItemController extends Controller
{
    public function index()
    {
        return Item::all();
    }

    public function store(ItemRequest $request)
    {
        $item = Item::create($request->all());

        return response()->json($item, 201);
    }

    public function show(Item $item)
    {
        return $item;
    }

    public function update(ItemRequest $request, Item $item)
    {
        $item->update($request->all());

        return response()->json($item, 200);
    }

    public function destroy(Item $item)
    {
        $item->delete();

        return response()->json(null, 204);
    }
}
