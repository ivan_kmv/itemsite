<?php

use Illuminate\Database\Seeder;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 5; $i++) {
            \App\Item::create([
                'name' => $faker->word,
                'key' => 'key'.($i+1),
            ]);
        }
    }
}
